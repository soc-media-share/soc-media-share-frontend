import { useAtom } from "jotai";
import { ReactNode } from "react";
import { BiSelectMultiple, BiLockAlt, BiPencil, BiCustomize, BiMessageAltCheck } from "react-icons/bi";

import * as steps from "../../Constants/HeaderSteps";

import { HeaderStep } from "../../Models/HeaderSteps";
import { headerStepAtom } from "../../store/header/header";

import "./Header.scss";

const iconSize = 22;

const HeaderStepIcons: { [key in HeaderStep]: ReactNode } = {
    [steps.SELECTION]: <BiSelectMultiple size={iconSize} />,
    [steps.AUTHENTICATION]: <BiLockAlt size={iconSize} />,
    [steps.INPUT]: <BiPencil size={iconSize} />,
    [steps.PREVIEW]: <BiCustomize size={iconSize} />,
    [steps.DONE]: <BiMessageAltCheck size={iconSize} />,
};

const HeaderStepIconKeys = Object.keys(HeaderStepIcons);

export const Header = () => {
    const [headerStep] = useAtom(headerStepAtom);

    return (
        <>
            <header>Social Media Share</header>
            <div className="boundary">
                <div className="header-stepper-container">
                    {HeaderStepIconKeys.map((name, index) => (
                        <div className={`header-stepper ${HeaderStepIconKeys.indexOf(headerStep) >= index ? "selected" : ""}`} key={name}>
                            {index !== 0 ? <div className="header-stepper__separator"></div> : null}
                            <div className="header-stepper__icon">{HeaderStepIcons[name as HeaderStep]}</div>
                        </div>
                    ))}
                </div>
            </div>
        </>
    );
};
