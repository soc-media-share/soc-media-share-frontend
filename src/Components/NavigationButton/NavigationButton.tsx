import { NavLink } from "react-router-dom";

type NavigationButtonType = {
    children: JSX.Element;
    handleClick?: () => any;
    to: string;
    disabled?: boolean;
};

export const NavigationButton = ({ children, handleClick, to, disabled = false }: NavigationButtonType) => {
    return disabled ? (
        children
    ) : (
        <NavLink onClick={handleClick} to={to}>
            {children}
        </NavLink>
    );
};
