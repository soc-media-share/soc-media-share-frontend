import { useMemo, useState, useEffect, forwardRef, ForwardedRef, useImperativeHandle } from "react";

import "./Tabs.scss";

type PropTypes<KeyT> = {
    tabItems: KeyT;
    onChange?: (keys: keyof KeyT) => any;
    positiveFeedBackTabs: (keyof KeyT)[];
    negativeFeedbackTab: (keyof KeyT)[];
};

export type TabController = {
    setTab: (newSelectedTab: any) => void;
};

// generic forward ref sucks
// TODO: look for better solution
export const Tabs = forwardRef<any, any>(<KeyT extends {}>(props: PropTypes<KeyT>, forwardedRef: ForwardedRef<TabController>) => {
    const positiveFeedBack = props.positiveFeedBackTabs ?? [];
    const negativeFeedbackTab = props.positiveFeedBackTabs ?? [];
    const keys = useMemo(() => Object.keys(props.tabItems) as (keyof typeof props.tabItems)[], [props.tabItems]);
    const [activeTab, setActiveTab] = useState(keys[0]);

    useImperativeHandle(
        forwardedRef,
        () => ({
            setTab: (newTab) => {
                setActiveTab(newTab);
                return newTab;
            },
        }),
        [setActiveTab]
    );

    useEffect(() => {
        if (props.onChange) props.onChange(activeTab);
    }, [activeTab]);

    useEffect(() => {
        if (!props.tabItems.hasOwnProperty(activeTab)) {
            setActiveTab(keys[0]);
            if (props.onChange) props.onChange(activeTab);
        }
    }, [props.tabItems]);

    return (
        <div className="tabs">
            {keys.map((key) => (
                <div
                    className={`tab-selector ${activeTab === key ? "active" : ""} ${
                        positiveFeedBack.includes(key) ? "tab-selector--green-border" : ""
                    } ${negativeFeedbackTab.includes(key) ? "tab-selector--red-border" : "tab-selector--red-border"}`}
                    onClick={() => setActiveTab(key)}
                    key={key as React.Key}
                >
                    {props.tabItems[key] as JSX.Element}
                </div>
            ))}
        </div>
    );
});
