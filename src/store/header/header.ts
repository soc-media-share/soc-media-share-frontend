import { SELECTION } from "../../Constants/HeaderSteps";

import { atom } from "jotai";
import { HeaderStep } from "@/Models/HeaderSteps";

export const headerStepAtom = atom<HeaderStep>(SELECTION);
