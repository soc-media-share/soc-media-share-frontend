import { createAction, createAsyncThunk, createReducer, createSelector } from "@reduxjs/toolkit";

import { getDeviantArtAccessToken, refreshDeviantArtAccessToken } from "../../api/deviantArt/token";
import { socialMediaKeyList } from "../../Constants/SocialMediaIcons";
import { Email } from "../../Models/Email";
import { Brands } from "../../Pages/MediaSelector/Models/brands";
import {
    addMissingElementsFromPrimitiveArrayToAnother,
    orderArrayWithArray,
    removePrimitiveArrayFromAnother,
    transformPrimitiveToArrayIfNotArray,
    wrappedIntoPayload,
} from "../../utils/StoreUtils";
import { RootState } from "../store";

// ACTIONS

// authorizedTabs actions
export const addAuthorizedTabs = createAction("authorizedTabs/add", (addedAuthorizedTabs: Brands | Brands[]) =>
    wrappedIntoPayload({
        newAuthorizedTabs: transformPrimitiveToArrayIfNotArray(addedAuthorizedTabs),
    })
);

export const removeAuthorizedTabs = createAction("authorizedTabs/remove", (deletedAuthorizedTabs: Brands | Brands[]) =>
    wrappedIntoPayload({
        deletedAuthorizedTabs: transformPrimitiveToArrayIfNotArray(deletedAuthorizedTabs),
    })
);

export const addPendingTabs = createAction("pendingTabs/add", (addedPendingTabs: Brands | Brands[]) =>
    wrappedIntoPayload({
        addedPendingTabs: transformPrimitiveToArrayIfNotArray(addedPendingTabs),
    })
);

export const removePendingTabs = createAction("pendingTabs/remove", (deletedPendingTabs: Brands | Brands[]) =>
    wrappedIntoPayload({
        deletedPendingTabs: transformPrimitiveToArrayIfNotArray(deletedPendingTabs),
    })
);

// renderedTabs actions
export const setRenderedTabs = createAction("renderedTabs/set", (newRenderedTabs: Brands[]) =>
    wrappedIntoPayload({
        newRenderedTabs: orderArrayWithArray(socialMediaKeyList, newRenderedTabs),
    })
);

export const removeRenderedTabs = createAction("renderedTabs/remove", (removedRenderedTabs: Brands[]) =>
    wrappedIntoPayload({
        removedRenderedTabs,
    })
);

// selectionsWithErrors actions
export const addSelectionsWithErrors = createAction("selectionsWithErrors/add", (newSelectionsWithErrors: Brands | Brands[]) =>
    wrappedIntoPayload({
        newSelectionsWithErrors: transformPrimitiveToArrayIfNotArray(newSelectionsWithErrors),
    })
);

export const removeSelectionsWithErrors = createAction("selectionsWithErrors/remove", (removedSelectionsWithErrors: Brands | Brands[]) =>
    wrappedIntoPayload({
        removedSelectionsWithErrors: transformPrimitiveToArrayIfNotArray(removedSelectionsWithErrors),
    })
);

// tabData actions
export const setSocialMedia = createAction("setSocialMedia/set", (brand: Brands, socialMediaData: Partial<SocialMediaData> = {}) =>
    wrappedIntoPayload({
        brand,
        [brand]: socialMediaData,
    })
);

export const setSocialMediaToken = createAction("setSocialMediaToken/set", (brand: Brands, token: string) =>
    wrappedIntoPayload({
        brand,
        token,
    })
);

export const setDeviantArtTokens = createAction("deviantArt/setDeviantArtToken", (accessToken: string, refreshToken: string) => {
    window.electron.saveToSafeStorage(refreshTokenExpirationDateKey, accessToken);
    window.electron.saveToSafeStorage(refreshTokenKey, refreshToken);

    return wrappedIntoPayload({
        accessToken,
        refreshToken,
    });
});

const refreshTokenKey = "deviantArtRefreshToken";
const refreshTokenExpirationDateKey = "refreshTokenExpirationDate";

export const refreshDeviantArtTokenThunk = createAsyncThunk("deviantArt/refreshDeviantArtTokenThunk", async (_, thunkAPI) => {
    const currentState = thunkAPI.getState() as RootState;
    const refreshToken = currentState.workflow.tabData.devianArt?.refreshToken;

    if (!refreshToken) {
        thunkAPI.abort("missing refresh token");
        return {};
    }

    const newTokenResponse = await refreshDeviantArtAccessToken(refreshToken);
    const newRefreshToken = newTokenResponse?.refresh_token;
    const newAccessToken = newTokenResponse?.access_token;

    if (!newRefreshToken || !newAccessToken) {
        thunkAPI.rejectWithValue({
            errorMessage: newTokenResponse?.error_description as string,
        });
        thunkAPI.abort(newTokenResponse?.error_description as string);
        return {};
    }

    thunkAPI.dispatch(setDeviantArtTokens(newAccessToken, newRefreshToken));

    return {
        token: newAccessToken,
        refreshToken: newRefreshToken,
    };
});

export const getDeviantArtTokenThunk = createAsyncThunk(
    "deviantArt/getDeviantArtTokenThunk",
    async (authToken: string): Promise<SocialMediaData | { errorMessage: string }> => {
        const accessTokenResponse = await getDeviantArtAccessToken(authToken);

        const newAccessToken = accessTokenResponse?.access_token;
        const newRefreshToken = accessTokenResponse?.refresh_token;

        if (!newAccessToken || !newRefreshToken) return {};

        return {
            token: newAccessToken,
            refreshToken: newRefreshToken,
        };
    }
);

// SELECTORS
const selectSelf = (state: RootState) => state.workflow;

export const authorizedTabsSelector = createSelector(selectSelf, (state) => state.authorizedTabs);
export const selectionsWithErrorsSelector = createSelector(selectSelf, (state) => state.selectionsWithErrors);
export const pendingTabsSelector = createSelector(selectSelf, (state) => state.pendingTabs);
export const renderedTabsSelector = createSelector(selectSelf, (state) => state.renderedTabs);
export const devianArtSelector = createSelector(selectSelf, (state) => state.tabData.devianArt);
export const devianArtTokenSelector = createSelector(selectSelf, (state) => state.tabData.devianArt?.token);
export const facebookSelector = createSelector(selectSelf, (state) => state.tabData.facebook);
export const instagramSelector = createSelector(selectSelf, (state) => state.tabData.instagram);
export const tiktokSelector = createSelector(selectSelf, (state) => state.tabData.tiktok);
export const twitterSelector = createSelector(selectSelf, (state) => state.tabData.twitter);
export const youtubeSelector = createSelector(selectSelf, (state) => state.tabData.youtube);

//REDUCER
interface State {
    authorizedTabs: Brands[];
    selectionsWithErrors: Brands[];
    pendingTabs: Brands[];
    renderedTabs: Brands[];
    tabData: TabData;
}

type interfaceOrEmail = string | Email;

type SocialMediaAuthentication = {
    usernameOrEmail?: interfaceOrEmail;
    password?: string;
};

type SocialMediaPost = {
    text?: string;
    tags?: string[];
};
type SocialMediaData = {
    authentication?: SocialMediaAuthentication;
    token?: string;
    refreshToken?: string;
    post?: SocialMediaPost;
};

type TabData = {
    [key in Brands]?: SocialMediaData;
};

const defaultState: State = {
    authorizedTabs: [],
    renderedTabs: [],
    pendingTabs: [],
    selectionsWithErrors: [],
    tabData: {},
};

export const workflowReducer = createReducer(defaultState, (builder) =>
    builder
        .addCase(addAuthorizedTabs, (state, action) => {
            state.authorizedTabs = addMissingElementsFromPrimitiveArrayToAnother(state.authorizedTabs, action.payload.newAuthorizedTabs);
        })
        .addCase(removeAuthorizedTabs, (state, action) => {
            state.authorizedTabs = removePrimitiveArrayFromAnother(state.authorizedTabs, action.payload.deletedAuthorizedTabs);
        })
        .addCase(addPendingTabs, (state, action) => {
            state.authorizedTabs = addMissingElementsFromPrimitiveArrayToAnother(state.authorizedTabs, action.payload.addedPendingTabs);
        })
        .addCase(removePendingTabs, (state, action) => {
            state.authorizedTabs = removePrimitiveArrayFromAnother(state.authorizedTabs, action.payload.deletedPendingTabs);
        })
        .addCase(setRenderedTabs, (state, action) => {
            state.renderedTabs = action.payload.newRenderedTabs;
        })
        .addCase(removeRenderedTabs, (state, action) => {
            state.renderedTabs = removePrimitiveArrayFromAnother(state.renderedTabs, action.payload.removedRenderedTabs);
        })
        .addCase(addSelectionsWithErrors, (state, action) => {
            state.authorizedTabs = addMissingElementsFromPrimitiveArrayToAnother(state.authorizedTabs, action.payload.newSelectionsWithErrors);
        })
        .addCase(removeSelectionsWithErrors, (state, action) => {
            state.authorizedTabs = removePrimitiveArrayFromAnother(state.authorizedTabs, action.payload.removedSelectionsWithErrors);
        })
        .addCase(setSocialMedia, (state, action) => {
            state.tabData[action.payload.brand] = { ...(action.payload[action.payload.brand] as {}) };
        })
        .addCase(setSocialMediaToken, (state, action) => {
            state.tabData[action.payload.brand] = {
                ...state.tabData[action.payload.brand],
                token: action.payload.token,
            };
        })
        .addCase(setDeviantArtTokens, (state, action) => {
            state.tabData.devianArt = {
                ...state.tabData.devianArt,
                refreshToken: action.payload.refreshToken,
                token: action.payload.accessToken,
            };
        })
        .addDefaultCase((_, action) => {
            new Error(`${action.type} case does not exist on workflow reducer`);
        })
);
