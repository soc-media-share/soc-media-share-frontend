export const transformPrimitiveToArrayIfNotArray = <T>(element: T | T[]) => {
    if (Array.isArray(element)) {
        return element;
    }

    return [element];
};

export const wrappedIntoPayload = <Payload extends {}>(payload: Payload) => ({
    payload: {
        ...payload,
    },
});

export const isObject = <T>(possibleObject: T) => possibleObject && typeof possibleObject === "object" && !Array.isArray(possibleObject);
export const valueIsFalsyButNotBoolean = <T>(value: T) => value !== false && !value;

export const deepMergeObjects = <T extends {}>(originObject: T, mergeObject: Partial<T>): T => {
    const originArrayKeyList = Object.keys(originObject) as (keyof typeof originObject)[];

    const mergedObject = originArrayKeyList.reduce((prevValue: T, nextPropKey): T => {
        if (isObject(mergeObject[nextPropKey])) {
            return {
                ...prevValue,
                [nextPropKey]: {
                    ...deepMergeObjects(originObject[nextPropKey] as {}, mergeObject[nextPropKey] as {}),
                },
            } as typeof originObject;
        }
        return { [nextPropKey]: valueIsFalsyButNotBoolean(mergeObject[nextPropKey]) ? originObject[nextPropKey] : mergeObject[nextPropKey] } as T;
    }, {} as T);

    return mergedObject;
};

export const orderArrayWithArray = <T>(orderList: T[], [...unorderedList]: T[]) =>
    unorderedList.sort((prevItem, nextItem) => orderList.indexOf(prevItem) - orderList.indexOf(nextItem));

export const addMissingElementsFromPrimitiveArrayToAnother = <T>(originArray: T[], additions: T[]) =>
    [...originArray, ...additions.filter((newElement) => !originArray.includes(newElement))] as T[];

export const removePrimitiveArrayFromAnother = <T>(originArray: T[], removableElements: T[]) =>
    originArray.filter((elements) => removableElements.some((deletedElements) => deletedElements === elements));
