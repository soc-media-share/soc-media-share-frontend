/**
 * @param  {{[key:string]:any;}} unparsedQueryParams this object cannot be nested
 */
export const convertToQueryParams = (unparsedQueryParams: {
    [key: string]: any;
}) => {
    const queryParamKeyList = Object.keys(unparsedQueryParams);
    return queryParamKeyList.reduce((prevValue, currentValue, index) => {
        const value = unparsedQueryParams[currentValue];

        let queryString = "";

        const andMarkIfNeeded = queryParamKeyList.length - 1 > index ? "&" : "";

        if (
            typeof currentValue === "string" ||
            typeof currentValue === "number"
        ) {
            queryString =
                prevValue + returnKeyAndValueAsString(currentValue, value);
        } else if (Array.isArray(value)) {
            queryString =
                prevValue +
                returnKeyAndValueAsString(
                    currentValue,
                    turnArrayIntoQueryString(value)
                );
        }

        return queryString + andMarkIfNeeded;
    }, "");
};

export const turnArrayIntoQueryString = (array: number[] | string[]) =>
    array.join(",");

export const turnQueryStringToArray = (queryString: string) =>
    queryString.split(",");

export const returnKeyAndValueAsString = (key: string, value: string) =>
    `${key}=${value}`;
