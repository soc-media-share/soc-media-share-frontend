export const snakeCaseToName = (key: string) => key.replace(/_/g, " ");

export const formatKeyToName = (key: string) => key.replace(/([A-Z])/g, " $1").trim();

export const formatPascalToKebabCase = (key: string) => key.replace(/([A-Z])/g, "-$1");

export const formatKebabCaseToPascal = (key: string) => key.replace(/-./g, (x) => x[1].toUpperCase());
