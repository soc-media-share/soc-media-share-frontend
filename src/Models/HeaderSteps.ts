export type HeaderStep = "SELECTION" | "AUTHENTICATION" | "INPUT" | "PREVIEW" | "DONE";
