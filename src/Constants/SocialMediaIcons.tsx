import { FaDeviantart, FaFacebookF, FaInstagram, FaTiktok, FaTwitter, FaYoutube } from "react-icons/fa";
import * as brands from "../Pages/MediaSelector/constants/brands";
import { Brands } from "../Pages/MediaSelector/Models/brands";

type BrandsElement = { [key in Brands]: JSX.Element };

export const mediaIconSizeGenerator = (size: number): BrandsElement => ({
    [brands.DEVIAN_ART]: <FaDeviantart size={size} />,
    [brands.FACEBOOK]: <FaFacebookF size={size} />,
    [brands.TWITTER]: <FaTwitter size={size} />,
    [brands.TIKTOK]: <FaTiktok size={size} />,
    [brands.YOUTUBE]: <FaYoutube size={size} />,
    [brands.INSTAGRAM]: <FaInstagram size={size} />,
});

export const smallMediaIcons: BrandsElement = mediaIconSizeGenerator(24);
export const largeMediaIcons: BrandsElement = mediaIconSizeGenerator(60);

export const socialMediaKeyList = Object.keys(smallMediaIcons) as (keyof BrandsElement)[];
