import { MemoryRouter, Route, Routes } from "react-router-dom";
import { Provider } from "react-redux";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";

import { Main } from "./Pages/Main/Pages/Main";

import { MediaSelector } from "./Pages/MediaSelector/Pages/MediaSelector/MediaSelector";
import { Instagram } from "./Pages/MediaAuthentication/Pages/Instagram/Instagram";
import { Youtube } from "./Pages/MediaAuthentication/Pages/Youtube/Youtube";
import { Tiktok } from "./Pages/MediaAuthentication/Pages/Tiktok/Tiktok";
import { Twitter } from "./Pages/MediaAuthentication/Pages/Twitter/Twitter";
import { Facebook } from "./Pages/MediaAuthentication/Pages/Facebook/Facebook";
import { DevianArt } from "./Pages/MediaAuthentication/Pages/DevianArt/DevianArt";
import { MediaAuthentication } from "./Pages/MediaAuthentication/Pages/MediaAuthentication/MediaAuthentication";
import { Posting } from "./Pages/Posting/Pages/Posting/Posting";
import { store } from "./store/store";

import "./App.scss";

const queryClient = new QueryClient();

function App() {
    const MediaSelectionStack = <Route path="media-selector" element={<MediaSelector />} />;

    const MediaAuthenticationStack = (
        <Route errorElement={<>error page</>} path="media-authentication" element={<MediaAuthentication />}>
            <Route
                loader={async () => await window.electron.getItemFromSafeStorage("deviantArtRefreshToken")}
                path="devian-art"
                element={<DevianArt />}
            />
            <Route path="facebook" element={<Facebook />} />
            <Route path="twitter" element={<Twitter />} />
            <Route path="tiktok" element={<Tiktok />} />
            <Route path="youtube" element={<Youtube />} />
            <Route path="instagram" element={<Instagram />} />
        </Route>
    );

    const PostingStack = <Route path="posting" element={<Posting />} />;

    return (
        <Provider store={store}>
            <QueryClientProvider client={queryClient}>
                <MemoryRouter>
                    <Routes>
                        <Route path="/" element={<Main />}>
                            {MediaSelectionStack}
                            {MediaAuthenticationStack}
                            {PostingStack}
                        </Route>
                    </Routes>
                </MemoryRouter>
                <ReactQueryDevtools />
            </QueryClientProvider>
        </Provider>
    );
}

declare global {
    interface Window {
        electron: {
            saveToSafeStorage: (key: string, data: string | number) => any;
            getItemFromSafeStorage: (key: string) => Promise<any>;
            removeItemFromStorage: (key: string) => any;
            openAuthWindow: (url: string) => any;
            closeAuthWindow: () => any;
            subscribeToToken: (cb: (token: string) => void) => any;
        };
    }
}

export default App;
