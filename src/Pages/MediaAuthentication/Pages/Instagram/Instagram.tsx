import { INSTAGRAM } from "../../../MediaSelector/constants/brands";

import { AuthenticationForm, SubmittedForm } from "../../Components/AuthenticationForm/AuthenticationForm";
import { useNavigateToNextSocialMediaOnStack } from "../../hooks/useNavigateToNextSocialMediaOnStack";

export const Instagram = () => {
    const navigateToSocialMedia = useNavigateToNextSocialMediaOnStack(INSTAGRAM, "./..");

    const handleFormSubmit = (form: SubmittedForm) => {
        // navigateToSocialMedia();
    };

    return <AuthenticationForm onSubmitForm={handleFormSubmit} />;
};
