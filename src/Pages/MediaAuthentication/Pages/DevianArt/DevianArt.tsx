import { useEffect, useState } from "react";

import "./DevianArt.scss";

import { DEVIAN_ART } from "../../../MediaSelector/constants/brands";
import { nextSocialMediaNavigation, useNavigateToNextSocialMediaOnStack } from "../../hooks/useNavigateToNextSocialMediaOnStack";
import { getAuthTokenURL, getDeviantArtAccessToken, refreshDeviantArtAccessToken, validateDeviantArtToken } from "../../../../api/deviantArt/token";
import { useSelector, useDispatch } from "react-redux";
import { devianArtTokenSelector, setSocialMediaToken } from "../../../../store/workflow/workflow";

const refreshTokenKey = "deviantArtRefreshToken";
const refreshTokenExpirationDateKey = "refreshTokenExpirationDate";

export const DevianArt = () => {
    const setSocialMediaAuthenticationState = useNavigateToNextSocialMediaOnStack(DEVIAN_ART, "./..");
    const devianArtToken = useSelector(devianArtTokenSelector);
    const [authToken, setAuthToken] = useState("");
    const [accessToken, setAccessToken] = useState(devianArtToken ?? "");

    const dispatch = useDispatch();

    const openAuthWindow = () => {
        window.electron.openAuthWindow(getAuthTokenURL);
    };

    useEffect(() => {
        handleAuthentication();

        window.electron.subscribeToToken((newToken: string) => setAuthToken(newToken));

        return () => {
            window.electron.closeAuthWindow();
        };
    }, []);

    useEffect(() => {
        if (authToken) {
            getAccessToken();
            window.electron.closeAuthWindow();
        }
    }, [authToken]);

    const handleAuthentication = async () => {
        if (accessToken) {
            const response = await validateDeviantArtToken(accessToken);
            const status = response?.status ?? "failed";

            if (status === "success") {
                setSocialMediaAuthenticationState(nextSocialMediaNavigation.SUCCESS);
                return;
            }
        }

        const refreshToken = await window.electron.getItemFromSafeStorage(refreshTokenKey);
        const refreshTokenExpirationDate = await window.electron.getItemFromSafeStorage(refreshTokenExpirationDateKey);

        if (refreshToken) {
            if (new Date(refreshTokenExpirationDate) > new Date()) refreshAccessToken(refreshToken);
            else openAuthWindow();
            return;
        }

        if (!authToken) openAuthWindow();
        else getAccessToken();
    };

    // TODO: handle errors
    const getAccessToken = async () => {
        const accessTokenResponse = await getDeviantArtAccessToken(authToken);

        if (accessTokenResponse?.refresh_token && accessTokenResponse?.access_token) {
            accessTokenFetchSuccess(accessTokenResponse.access_token, accessTokenResponse.refresh_token);
        } else {
            setSocialMediaAuthenticationState(nextSocialMediaNavigation.FAILURE);
            console.log("getAccessToken error: ", accessTokenResponse?.error_description);
        }
    };

    const refreshAccessToken = async (refreshToken: string) => {
        const newTokenResponse = await refreshDeviantArtAccessToken(refreshToken);

        if (newTokenResponse?.refresh_token && newTokenResponse?.access_token) {
            accessTokenFetchSuccess(newTokenResponse.access_token, newTokenResponse?.refresh_token);
        } else {
            setSocialMediaAuthenticationState(nextSocialMediaNavigation.FAILURE);
            console.log("getAccessToken error: ", newTokenResponse?.error_description);
        }
    };

    const accessTokenFetchSuccess = (accessToken: string, refreshToken: string) => {
        window.electron.saveToSafeStorage(refreshTokenKey, refreshToken);
        window.electron.saveToSafeStorage(refreshTokenExpirationDateKey, getNewAccessTokenExpirationDate());
        setAccessToken(accessToken);
        dispatch(setSocialMediaToken(DEVIAN_ART, accessToken));

        setSocialMediaAuthenticationState(nextSocialMediaNavigation.SUCCESS);
    };

    const getNewAccessTokenExpirationDate = () => {
        const date = new Date();
        const availableMonths = 3;
        return new Date(date.setMonth(date.getMonth() + availableMonths)).toString();
    };

    return (
        <div className="devian-art">
            <div className="button" onClick={openAuthWindow}>
                authentication
            </div>
        </div>
    );
};
