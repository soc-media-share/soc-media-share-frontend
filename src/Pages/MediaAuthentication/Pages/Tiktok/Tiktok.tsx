import { TIKTOK } from "../../../MediaSelector/constants/brands";

import { AuthenticationForm, SubmittedForm } from "../../Components/AuthenticationForm/AuthenticationForm";
import { useNavigateToNextSocialMediaOnStack } from "../../hooks/useNavigateToNextSocialMediaOnStack";

export const Tiktok = () => {
    const navigateToSocialMedia = useNavigateToNextSocialMediaOnStack(TIKTOK, "./..");

    const handleFormSubmit = (form: SubmittedForm) => {
        // navigateToSocialMedia();
    };

    return <AuthenticationForm onSubmitForm={handleFormSubmit} />;
};
