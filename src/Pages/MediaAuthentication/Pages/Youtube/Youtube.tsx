import { YOUTUBE } from "../../../MediaSelector/constants/brands";

import { AuthenticationForm, SubmittedForm } from "../../Components/AuthenticationForm/AuthenticationForm";
import { useNavigateToNextSocialMediaOnStack } from "../../hooks/useNavigateToNextSocialMediaOnStack";

export const Youtube = () => {
    const navigateToSocialMedia = useNavigateToNextSocialMediaOnStack(YOUTUBE, "./..");

    const handleFormSubmit = (form: SubmittedForm) => {
        // navigateToSocialMedia();
    };

    return <AuthenticationForm onSubmitForm={handleFormSubmit} />;
};
