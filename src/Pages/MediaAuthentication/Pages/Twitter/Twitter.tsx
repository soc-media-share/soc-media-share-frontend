import { TWITTER } from "../../../MediaSelector/constants/brands";

import { AuthenticationForm, SubmittedForm } from "../../Components/AuthenticationForm/AuthenticationForm";
import { useNavigateToNextSocialMediaOnStack } from "../../hooks/useNavigateToNextSocialMediaOnStack";

export const Twitter = () => {
    const navigateToSocialMedia = useNavigateToNextSocialMediaOnStack(TWITTER, "./..");

    const handleFormSubmit = (form: SubmittedForm) => {
        // navigateToSocialMedia();
    };

    return <AuthenticationForm onSubmitForm={handleFormSubmit} />;
};
