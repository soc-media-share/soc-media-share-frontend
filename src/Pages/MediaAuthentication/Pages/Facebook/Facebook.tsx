import { FACEBOOK } from "../../../MediaSelector/constants/brands";

import { AuthenticationForm, SubmittedForm } from "../../Components/AuthenticationForm/AuthenticationForm";
import { useNavigateToNextSocialMediaOnStack } from "../../hooks/useNavigateToNextSocialMediaOnStack";

export const Facebook = () => {
    const navigateToSocialMedia = useNavigateToNextSocialMediaOnStack(FACEBOOK, "./..");

    const handleFormSubmit = (form: SubmittedForm) => {
        // navigateToSocialMedia();
    };

    return <AuthenticationForm onSubmitForm={handleFormSubmit} />;
};
