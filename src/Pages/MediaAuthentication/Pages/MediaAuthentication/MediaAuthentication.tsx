import { useEffect, useMemo, useRef, useState } from "react";
import { Outlet, useLocation, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

import "./MediaAuthentication.scss";

import { AUTHENTICATION } from "../../../../Constants/HeaderSteps";
import { headerStepAtom } from "../../../../store/header/header";
import { authorizedTabsSelector, renderedTabsSelector } from "../../../../store/workflow/workflow";
import { Brands } from "../../../MediaSelector/Models/brands";
import { TabController, Tabs } from "../../../../Components/Tabs/Tabs";
import { formatKebabCaseToPascal, formatPascalToKebabCase } from "../../../../utils/NameUtils";
import { NavigationButton } from "../../../../Components/NavigationButton/NavigationButton";
import { smallMediaIcons, socialMediaKeyList } from "../../../../Constants/SocialMediaIcons";
import { useAtom } from "jotai";

type BrandsElement = { [key in Brands]?: JSX.Element };

export const MediaAuthentication = () => {
    const tabsRef = useRef<TabController>();
    const renderedTabs = useSelector(renderedTabsSelector);
    const authorizedTabs = useSelector(authorizedTabsSelector);

    const location = useLocation();
    const navigate = useNavigate();

    const [_, setHeaderStep] = useAtom(headerStepAtom);
    const [activeTab, setActiveTab] = useState<Brands>();

    const renderedTabsElements = useMemo(
        () =>
            socialMediaKeyList.reduce<BrandsElement>((prevValue, currentValue) => {
                if (!renderedTabs.includes(currentValue)) return prevValue;
                return {
                    ...prevValue,
                    [currentValue]: smallMediaIcons[currentValue],
                };
            }, {}),
        [renderedTabs]
    );

    const navigateToBrand = (destinationBrand: Brands) => {
        navigate(`./${formatPascalToKebabCase(destinationBrand).toLocaleLowerCase()}`);
    };

    useEffect(() => {
        setHeaderStep(AUTHENTICATION);
    }, []);

    useEffect(() => {
        const paths = location.pathname.split("/");
        const currentSocialMedia = paths[paths.length - 1];
        if (activeTab !== currentSocialMedia && paths.length > 2) {
            tabsRef.current?.setTab(formatKebabCaseToPascal(currentSocialMedia));
        }
    }, [location]);

    const handleTabChange = (newActiveTab: Brands) => {
        if (!location.pathname.includes(formatPascalToKebabCase(newActiveTab).toLocaleLowerCase())) {
            navigateToBrand(newActiveTab);
        }

        setActiveTab(newActiveTab);
    };

    return (
        <>
            <Tabs
                ref={tabsRef as React.ForwardedRef<TabController>}
                tabItems={renderedTabsElements}
                onChange={handleTabChange}
                positiveFeedBackTabs={authorizedTabs}
            />
            <div className="tab-container">
                <Outlet />
            </div>
            <div className="next-button-container">
                <div className="back">
                    <NavigationButton to="/media-selector">
                        <div className="button">back</div>
                    </NavigationButton>
                </div>
                <NavigationButton to="/input">
                    <div className="button ">next</div>
                </NavigationButton>
            </div>
        </>
    );
};
