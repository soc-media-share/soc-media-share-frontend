import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import {
    addAuthorizedTabs,
    addPendingTabs,
    addSelectionsWithErrors,
    authorizedTabsSelector,
    pendingTabsSelector,
    removeAuthorizedTabs,
    removePendingTabs,
    removeSelectionsWithErrors,
    renderedTabsSelector,
    selectionsWithErrorsSelector,
} from "../../../store/workflow/workflow";
import { formatPascalToKebabCase } from "../../../utils/NameUtils";
import { Brands } from "../../MediaSelector/Models/brands";

export type NextSocialMediaNavigation = "SUCCESS" | "FAILURE" | "PENDING";

export const nextSocialMediaNavigation: { [key in NextSocialMediaNavigation]: NextSocialMediaNavigation } = {
    SUCCESS: "SUCCESS",
    FAILURE: "FAILURE",
    PENDING: "PENDING",
};

export const useNavigateToNextSocialMediaOnStack = (currentSocialMedia: Brands, navigationBeforeSocialMedia: string) => {
    const tabs = useSelector(renderedTabsSelector);
    const authorizedTabs = useSelector(authorizedTabsSelector);
    const selectionsWithErrors = useSelector(selectionsWithErrorsSelector);
    const pendingTabs = useSelector(pendingTabsSelector);
    const [nextSocialMediaSiteIndex, setNextSocialMediaSiteIndex] = useState<number>(0);
    const [currentSocialMediaAuthenticationState, setCurrentSocialMediaAuthenticationState] = useState<NextSocialMediaNavigation>(
        nextSocialMediaNavigation.PENDING
    );

    const navigate = useNavigate();
    const dispatch = useDispatch();

    useEffect(() => {
        const nextFilteredTabIndex = tabs.findIndex((tab) => !authorizedTabs.includes(tab) && currentSocialMedia !== tab);
        setNextSocialMediaSiteIndex(nextFilteredTabIndex);
    }, [tabs, authorizedTabs, currentSocialMedia]);

    useEffect(() => {
        navigateToNextSocialMedia(currentSocialMediaAuthenticationState);
    }, [currentSocialMediaAuthenticationState]);

    const navigateToNextSocialMedia = (currentSocialMediaStatus: NextSocialMediaNavigation) => {
        if (tabs.length <= 1) return;
        const currentSocialMedia = tabs[nextSocialMediaSiteIndex - 1];

        if (!currentSocialMedia) return;

        switch (currentSocialMediaStatus) {
            case nextSocialMediaNavigation.FAILURE:
                dispatch(removePendingTabs(currentSocialMedia));
                dispatch(removeAuthorizedTabs(currentSocialMedia));
                if (!selectionsWithErrors.includes(currentSocialMedia)) dispatch(addSelectionsWithErrors(currentSocialMedia));
                break;
            case nextSocialMediaNavigation.SUCCESS:
                dispatch(removePendingTabs(currentSocialMedia));
                dispatch(removeSelectionsWithErrors(currentSocialMedia));
                if (!authorizedTabs.includes(currentSocialMedia)) dispatch(addAuthorizedTabs(currentSocialMedia));

                navigate(`${navigationBeforeSocialMedia}/${formatPascalToKebabCase(tabs[nextSocialMediaSiteIndex] as string).toLocaleLowerCase()}`);
                break;
            case nextSocialMediaNavigation.PENDING:
                dispatch(removeAuthorizedTabs(currentSocialMedia));
                dispatch(removeSelectionsWithErrors(currentSocialMedia));
                if (!pendingTabs.includes(currentSocialMedia)) dispatch(addPendingTabs(currentSocialMedia));
                break;
            default:
                dispatch(removeAuthorizedTabs(currentSocialMedia));
                dispatch(removeSelectionsWithErrors(currentSocialMedia));
                if (!pendingTabs.includes(currentSocialMedia)) dispatch(addPendingTabs(currentSocialMedia));
                break;
        }
    };

    return setCurrentSocialMediaAuthenticationState;
};
