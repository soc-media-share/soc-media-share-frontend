import { useRef, useLayoutEffect } from "react";

import "./AuthenticationForm.scss";

export type SubmittedForm = {
    username: string;
    password: string;
};

type PropTypes = {
    onSubmitForm: (submittedForm: SubmittedForm) => any;
};

export const AuthenticationForm = (props: PropTypes) => {
    const userNameRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);

    useLayoutEffect(() => {
        userNameRef.current?.focus();
    }, []);

    const handleFormSubmit: React.FormEventHandler<HTMLFormElement> = (e) => {
        e.preventDefault();
        props.onSubmitForm({
            username: userNameRef?.current?.value as string,
            password: passwordRef?.current?.value as string,
        });
    };

    return (
        <form onSubmit={handleFormSubmit} className="authentication-form">
            <div className="form-input-group">
                <label htmlFor="username">Username:</label>
                <input id="username" ref={userNameRef} type="text" name="username" />
            </div>
            <div className="form-input-group">
                <label htmlFor="password">Password:</label>
                <input id="password" ref={passwordRef} type="password" name="username" />
            </div>

            <button className="button button--wide" type="submit">
                Login
            </button>
        </form>
    );
};
