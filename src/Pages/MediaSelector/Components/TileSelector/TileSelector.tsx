import { TiTick } from "react-icons/ti";

import { TileKey, TilesType } from "../../Models/Tiles";
import { Brands } from "../../Models/brands";

import "./TileSelector.scss";
import { formatKeyToName } from "../../../../utils/NameUtils";
import { useKeyAsName } from "../../hooks/useKeyAsName/useKeyAsName";

type PropTypes = {
    tiles: TilesType;
    selectedTiles: Brands[];
    changeSelectedTiles: (newSelectedTiles: TileKey[]) => void;
};

export const TileSelector = (props: PropTypes) => {
    const toggleSocialMedia = (key: Brands) => {
        const oldSelectedTiles = [...props.selectedTiles];

        if (oldSelectedTiles.includes(key)) {
            oldSelectedTiles.splice(oldSelectedTiles.indexOf(key), 1);
        } else {
            oldSelectedTiles.push(key);
        }

        props.changeSelectedTiles(oldSelectedTiles);
    };

    const [brandList, tileNames] = useKeyAsName(props.tiles, formatKeyToName);

    return (
        <div className="tile-container">
            {brandList.map((tileKey) => (
                <div key={tileKey} onClick={() => toggleSocialMedia(tileKey)} className="tile media-tile">
                    <div className={`media-tile__tick ${props.selectedTiles.includes(tileKey) ? "media-tile__tick--active" : ""}`}>
                        <TiTick size={30} color="#187018" />
                    </div>
                    {props.tiles[tileKey]}
                    <div className="media-tile__name">{tileNames[tileKey]}</div>
                </div>
            ))}
        </div>
    );
};
