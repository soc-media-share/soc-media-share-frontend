import * as brands from "../../constants/brands";
import { useKeyAsName } from "../../hooks/useKeyAsName/useKeyAsName";
import { Brands } from "../../Models/brands";
import { isPrimitivesArraySubgroupOfOtherArray } from "../../utils/arrayUtils";
import { snakeCaseToName } from "../../../../utils/NameUtils";

import "./SelectorButtons.scss";

const TEXT_SOCIAL_MEDIA: Brands[] = [brands.FACEBOOK, brands.INSTAGRAM, brands.TWITTER];
const IMAGE_SOCIAL_MEDIA: Brands[] = [brands.FACEBOOK, brands.INSTAGRAM, brands.TWITTER, brands.DEVIAN_ART];
const VIDEO_SOCIAL_MEDIA: Brands[] = [brands.FACEBOOK, brands.INSTAGRAM, brands.TWITTER, brands.YOUTUBE, brands.TIKTOK];

const SELECT_ALL: Brands[] = [brands.DEVIAN_ART, brands.FACEBOOK, brands.INSTAGRAM, brands.TWITTER, brands.YOUTUBE, brands.TIKTOK];

type Buttons = {
    [key: string]: Brands[];
};

const buttons: Buttons = {
    select_all: SELECT_ALL,
    with_text: TEXT_SOCIAL_MEDIA,
    with_image: IMAGE_SOCIAL_MEDIA,
    with_video: VIDEO_SOCIAL_MEDIA,
};

type PropTypes = {
    selectedTiles: Brands[];
    changeSelectedTiles: React.Dispatch<React.SetStateAction<Brands[]>>;
};

export function SelectorButtons(props: PropTypes) {
    const addTileSelections = (selectedTiles: Brands[]) => {
        props.changeSelectedTiles((prevSelectedTiles) => {
            let newSelectedTiles = [...prevSelectedTiles];

            const alreadyAllTilesAreSelected = selectedTiles.every((selectedTile) => newSelectedTiles.includes(selectedTile));

            if (alreadyAllTilesAreSelected) {
                newSelectedTiles = deselectTiles(newSelectedTiles, selectedTiles);
            } else {
                newSelectedTiles = addAllUnselectedTiles(newSelectedTiles, selectedTiles);
            }

            return newSelectedTiles;
        });
    };

    const deselectTiles = ([...newSelectedTiles]: Brands[], [...selectedTiles]: Brands[]) => {
        selectedTiles.forEach((selectedTile) => {
            if (newSelectedTiles.includes(selectedTile)) {
                newSelectedTiles.splice(newSelectedTiles.indexOf(selectedTile), 1);
            }
        });

        return newSelectedTiles;
    };

    const addAllUnselectedTiles = ([...newSelectedTiles]: Brands[], [...selectedTiles]: Brands[]) => {
        selectedTiles.forEach((selectedTile) => {
            if (!newSelectedTiles.includes(selectedTile)) {
                newSelectedTiles.push(selectedTile);
            }
        });

        return newSelectedTiles;
    };

    const [buttonsList, buttonNames] = useKeyAsName<Buttons>(buttons, snakeCaseToName);

    return (
        <div className="custom-selector-container">
            {buttonsList.map((buttonsListKey: keyof Buttons) => (
                <button
                    key={buttonsListKey}
                    className={`button selector-button ${
                        isPrimitivesArraySubgroupOfOtherArray(props.selectedTiles, buttons[buttonsListKey]) ? "selected" : ""
                    }`}
                    onClick={() => addTileSelections(buttons[buttonsListKey])}
                >
                    {buttonNames[buttonsListKey]}
                </button>
            ))}
        </div>
    );
}
