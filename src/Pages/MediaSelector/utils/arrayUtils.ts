export const isPrimitivesArraySubgroupOfOtherArray = (
    array: any[],
    subArray: any[]
) =>
    subArray.every((element) => {
        return array.includes(element);
    });
