import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useAtom } from "jotai";

import { TileSelector } from "../../Components/TileSelector/TileSelector";
import { SelectorButtons } from "../../Components/SelectorButtons/SelectorButtons";
import { headerStepAtom } from "../../../../store/header/header";
import { setRenderedTabs } from "../../../../store/workflow/workflow";
import { NavigationButton } from "../../../../Components/NavigationButton/NavigationButton";
import { SELECTION } from "../../../../Constants/HeaderSteps";
import { TileKey } from "../../Models/Tiles";

import "./MediaSelector.scss";
import { largeMediaIcons } from "../../../../Constants/SocialMediaIcons";

export const MediaSelector = () => {
    const [selectedTiles, setSelectedTiles] = useState<Array<TileKey>>([]);
    const dispatch = useDispatch();

    const [_, setHeaderStep] = useAtom(headerStepAtom);

    useEffect(() => {
        setHeaderStep(SELECTION);
    }, []);

    return (
        <div className="media-selector">
            <SelectorButtons selectedTiles={selectedTiles} changeSelectedTiles={setSelectedTiles} />
            <TileSelector tiles={largeMediaIcons} selectedTiles={selectedTiles} changeSelectedTiles={setSelectedTiles} />
            <div className="next-button-container">
                <NavigationButton
                    disabled={!selectedTiles.length}
                    to="/media-authentication"
                    handleClick={() => dispatch(setRenderedTabs(selectedTiles))}
                >
                    <div className="next button">next</div>
                </NavigationButton>
            </div>
        </div>
    );
};
