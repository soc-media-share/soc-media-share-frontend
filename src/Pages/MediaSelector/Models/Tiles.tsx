import { Brands } from "./brands";

export type TilesType = {
    [key in Brands]: JSX.Element;
};

export type TileKey = keyof TilesType;
