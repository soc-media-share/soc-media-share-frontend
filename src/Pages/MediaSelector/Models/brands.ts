import * as brands from "../constants/brands";

export type Brands =
    | typeof brands.DEVIAN_ART
    | typeof brands.FACEBOOK
    | typeof brands.TWITTER
    | typeof brands.TIKTOK
    | typeof brands.YOUTUBE
    | typeof brands.INSTAGRAM;
