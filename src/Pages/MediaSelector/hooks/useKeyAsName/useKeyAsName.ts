import { useMemo } from "react";

type Formatter = (key: string) => string;

export const useKeyAsName = <T extends {}>(
    keys: T,
    formatters: Formatter[] | Formatter
): [(keyof T)[], { [key in keyof T]: string }] => {
    const list = useMemo(() => Object.keys(keys), []);

    const formatValue = (value: string) => {
        if (typeof formatters !== "function") {
            let newValue = value;
            formatters.forEach((formatter) => {
                newValue = formatter(newValue);
            });

            return newValue;
        } else {
            return formatters(value);
        }
    };

    const names = useMemo(
        () =>
            list.reduce(
                (prevValue, currentValue) => ({
                    ...prevValue,
                    [currentValue]: formatValue(currentValue),
                }),
                {} as { [key in keyof T]: string }
            ),
        []
    );

    return [list as (keyof T)[], names];
};
