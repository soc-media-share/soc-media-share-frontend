import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Outlet, useNavigate } from "react-router-dom";

import { Header } from "../../../Layout/Header/Header";
import { devianArtTokenSelector, refreshDeviantArtTokenThunk } from "../../../store/workflow/workflow";

import "./Main.scss";

export const Main = () => {
    const navigate = useNavigate();
    const deviantArtToken = useSelector(devianArtTokenSelector);
    const dispatch = useDispatch();
    const [time, setTime] = useState(true);

    useEffect(() => {
        if (deviantArtToken && time) {
            subscribeToDeviantTokenRefresh();
            // setTimeout(() => {}, 3600000);
        }
    }, [deviantArtToken]);

    const subscribeToDeviantTokenRefresh = async () => {
        setTime(false);
        const refresh = await dispatch(refreshDeviantArtTokenThunk() as any);
        console.log(refresh);
    };

    useEffect(() => {
        navigate("/media-selector");
    }, []);

    return (
        <>
            <Header />
            <main className="layout">
                <Outlet />
            </main>
        </>
    );
};
