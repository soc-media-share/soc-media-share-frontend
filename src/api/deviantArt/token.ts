export type AccessTokenSuccessResponse = {
    expires_in: 3600;
    access_token: string;
    token_type: string;
    refresh_token: string;
    scope: string;
};

export type AccessTokenErrorResponse = {
    error: string;
    error_description: string;
};

const clientID = import.meta.env.VITE_APP_DEVIANT_ART_CLIENT_ID;
const clientSecret = import.meta.env.VITE_APP_DEVIANT_ART_SECRET;

export type DeviantArtResponse = Partial<AccessTokenSuccessResponse & AccessTokenErrorResponse>;

// TODO: error handling
export const getDeviantArtAccessToken = async (token: string): Promise<DeviantArtResponse | undefined> => {
    try {
        const response = await fetch(
            `/deviant-art-oauth2/token?grant_type=authorization_code&client_id=${clientID}&client_secret=${clientSecret}&redirect_uri=${
                import.meta.env.VITE_APP_REDIRECT_TO
            }&code=${token}`
        );

        return response.json();
    } catch (error) {
        console.log(error);
    }
};

export const refreshDeviantArtAccessToken = async (refreshToken: string): Promise<DeviantArtResponse | undefined> => {
    try {
        const response = await fetch(
            `/deviant-art-oauth2/token?grant_type=refresh_token&client_id=${clientID}&client_secret=${clientSecret}&refresh_token=${refreshToken}`
        );

        return response.json();
    } catch (error) {
        console.log(error);
    }
};

export type ValidTokenResponse = {
    status: string;
};

export const validateDeviantArtToken = async (accessToken: string): Promise<ValidTokenResponse | undefined> => {
    try {
        const response = await fetch(`/placebo?access_token=${accessToken}`);

        return await response.json();
    } catch (error) {
        console.log(error);
    }
};

export const getAuthTokenURL = `http://www.deviantart.com/oauth2/authorize?response_type=code&client_id=${
    import.meta.env.VITE_APP_DEVIANT_ART_CLIENT_ID
}&redirect_uri=${import.meta.env.VITE_APP_REDIRECT_TO}`;
