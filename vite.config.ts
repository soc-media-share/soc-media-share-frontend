import { rmSync } from "fs";
import path from "path";
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import electron from "vite-electron-plugin";
import { customStart, loadViteEnv } from "vite-electron-plugin/plugin";
import pkg from "./package.json";

rmSync(path.join(__dirname, "dist-electron"), { recursive: true, force: true });

// https://vitejs.dev/config/
export default defineConfig({
    resolve: {
        alias: {
            "@": path.join(__dirname, "src"),
            styles: path.join(__dirname, "src/assets/styles"),
        },
    },
    plugins: [
        react(),
        electron({
            include: ["electron", "preload"],
            transformOptions: {
                sourcemap: !!process.env.VSCODE_DEBUG,
            },
            plugins: [
                ...(process.env.VSCODE_DEBUG
                    ? [
                          // Will start Electron via VSCode Debug
                          customStart(debounce(() => console.log(/* For `.vscode/.debug.script.mjs` */ "[startup] Electron App"))),
                      ]
                    : []),
                // Allow use `import.meta.env.VITE_SOME_KEY` in Electron-Main
                loadViteEnv(),
            ],
        }),
    ],
    server: process.env.VSCODE_DEBUG
        ? (() => {
              const url = new URL(pkg.debug.env.VITE_DEV_SERVER_URL);
              return {
                  host: url.hostname,
                  port: +url.port,
              };
          })()
        : {
              proxy: {
                  "/deviant-art-oauth2": {
                      target: "https://www.deviantart.com/oauth2",
                      changeOrigin: true,
                      secure: false,
                      rewrite: (path) => path.replace("/deviant-art-oauth2", ""),
                      configure: (proxy, _options) => {
                          proxy.on("error", (err, _req, _res) => {
                              console.log("deviant art error", err);
                          });
                          proxy.on("proxyReq", (proxyReq, req, _res) => {
                              console.log("Sending Request to deviant art:", req.method, req.url);
                          });
                          proxy.on("proxyRes", (proxyRes, req, _res) => {
                              console.log("Received Response from deviant art:", proxyRes.statusCode, req.url);
                          });
                      },
                  },
                  "/placebo": "https://www.deviantart.com/api/v1/oauth2",
              },
          },
    clearScreen: false,
});

function debounce<Fn extends (...args: any[]) => void>(fn: Fn, delay = 299) {
    let t: NodeJS.Timeout;
    return ((...args) => {
        clearTimeout(t);
        t = setTimeout(() => fn(...args), delay);
    }) as Fn;
}
