import { contextBridge, ipcRenderer } from "electron";

contextBridge.exposeInMainWorld("electron", {
    saveToSafeStorage: (key, data) => ipcRenderer.send("save-item-to-storage", key, data),
    getItemFromSafeStorage: (key) => ipcRenderer.invoke("get-item-from-storage", key).then((result) => result),
    removeItemFromStorage: (key) => ipcRenderer.send("remove-item-from-storage", key),
    openAuthWindow: (url) => ipcRenderer.send("open-auth-window", url),
    closeAuthWindow: () => ipcRenderer.send("close-auth-window"),
    subscribeToToken: (cb) => ipcRenderer.on("social-media-token", (_, token) => cb(token)),
});
