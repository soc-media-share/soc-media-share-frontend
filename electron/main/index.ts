import path from "path";

import { app, BrowserWindow, crashReporter } from "electron";

const gotTheLock = app.requestSingleInstanceLock();

import { createWindow } from "../electronUtils/createMainWindow";

import { release } from "os";
import logger from "../electronUtils/logger";

// Disable GPU Acceleration for Windows 7
if (release().startsWith("6.1")) app.disableHardwareAcceleration();
// Set application name for Windows 10+ notifications
if (process.platform === "win32") app.setAppUserModelId(app.getName());

// open app through browser with soc-media-share://<?params=values>
if (process.defaultApp) {
    if (process.argv.length >= 2) {
        app.setAsDefaultProtocolClient("soc-media-share", process.execPath, [path.resolve(process.argv[1])]);
    }
} else {
    app.setAsDefaultProtocolClient("soc-media-share");
}

if (!gotTheLock) {
    app.quit();
    process.exit(0);
}

let win: BrowserWindow | null = null;

app.on("second-instance", (event, [...commandLine], workingDirectory) => {
    if (win) {
        if (win.isMinimized()) win.restore();
        win.focus();

        commandLine.splice(0, 3);
    }
});

app.on("open-url", (e, url) => {
    // logger.log("url: ", url);

    handleDeepLinkWithParams(url);
});

const handleDeepLinkWithParams = (url: string) => {
    const [_, param] = url.split("?");

    if (typeof param === "string") {
        const [key, value] = param.split("=");
        if (key === "code") {
            win.webContents.send("social-media-token", value);
        }
    }
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
    win = createWindow();
    crashReporter.start({ uploadToServer: false });
});

app.on("activate", () => {
    const allWindows = BrowserWindow.getAllWindows();
    if (allWindows.length) {
        allWindows[0].focus();
    } else {
        createWindow();
    }
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
    win = null;
    if (process.platform !== "darwin") app.quit();
});
