import electronLogger from "electron-log";
import path from "path";

electronLogger.transports.file.resolvePath = () => path.join(__dirname, "logs/main.log");
electronLogger.transports.file.level = "info";

export default electronLogger;
