import logger from "./logger";

import { BrowserWindow } from "electron";
// "http://www.deviantart.com//oauth2/authorize?response_type=code&client_id=22226&redirect_uri=soc-media-share%3A%2F%2F"

export class AuthWindow {
    url: string;
    browserWindowParams: Electron.BrowserWindowConstructorOptions = {};
    loginBrowserWindow: Electron.BrowserWindow;
    isOpen = false;

    constructor(url = "", browserWindowParams = {}) {
        this.url = url;
        this.browserWindowParams = browserWindowParams;
    }

    openWindow() {
        if (this.isOpen) {
            this.loginBrowserWindow.focus();
            return;
        }
        this.loginBrowserWindow = new BrowserWindow({
            width: 420,
            height: 660,
            webPreferences: {
                nodeIntegration: true,
            },
            ...this.browserWindowParams,
        });

        this.loginBrowserWindow.on("close", () => {
            this.isOpen = false;
        });

        try {
            // shell.openExternal(this.url);
            this.loginBrowserWindow.loadURL(this.url);
            // TODO: handle error on webContents load error
            this.loginBrowserWindow.webContents.on("did-fail-load", (e, errorCode, errorDescription) => {
                logger.error("failed loading window", errorCode, errorDescription);
            });
            this.isOpen = true;
        } catch (error) {
            logger.error("error happened: ", error);
        }
        logger.log("OPEN_AUTH_WINDOW: ", this.url);
    }

    setUrl(url: string) {
        this.url = url;
        return this;
    }

    closeWindow() {
        if (!this.isOpen) return;
        this.loginBrowserWindow.close();
    }
}
