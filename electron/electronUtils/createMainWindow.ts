import { BrowserWindow, ipcMain, app } from "electron";
import path from "path";
import { AuthWindow } from "./AuthWindow";
import { SecretStorage } from "./SecretStorage";
import { URL } from "url";
import logger from "./logger";

process.env.DIST_ELECTRON = path.join(__dirname, "/../..");
process.env.DIST = path.join(process.env.DIST_ELECTRON, "../dist");
process.env.PUBLIC = app.isPackaged ? process.env.DIST : path.join(process.env.DIST_ELECTRON, "../public");

const preload = path.join(__dirname, "../preload/index.js");
const indexHtml = path.join(process.env.DIST, "index.html");

export const createWindow = () => {
    const authWindows = new AuthWindow();
    const secretStorage = new SecretStorage();

    secretStorage.loadStorage();

    // Create the browser window.
    const win = new BrowserWindow({
        width: 1000,
        height: 700,
        webPreferences: {
            nodeIntegration: true,
            preload,
        },
        resizable: false,
        titleBarStyle: "hidden",
        titleBarOverlay: {
            color: "#3d3a3a",
            height: 20,
        },
    });

    // ipcMain.emit();

    ipcMain.on("open-auth-window", (event, url, secretKey) => {
        authWindows.setUrl(url);
        authWindows.openWindow();
    });

    ipcMain.on("close-auth-window", () => {
        // logger.log("CLOSE_AUTH_WINDOW");
        authWindows.closeWindow();
    });

    ipcMain.handle("get-item-from-storage", (event, key) => {
        // logger.log("GET_ITEM_FROM_STORAGE: ", key);
        return secretStorage.getItem(key);
    });

    ipcMain.on("save-item-to-storage", (event, key, data) => {
        // logger.log("SAVE_ITEM_TO_STORAGE: ", key);
        secretStorage.updateStorage(key, data);
    });

    ipcMain.on("remove-item-from-storage", (event, key) => {
        secretStorage.removeItemFromStorage(key);
    });

    win.setMenuBarVisibility(false);
    // and load the index.html of the app.
    // win.loadFile("index.html");
    if (process.env.VITE_DEV_SERVER_URL) {
        // electron-vite-vue#298
        const loadUrl = new URL(process.env.VITE_DEV_SERVER_URL, __dirname);
        win.loadURL(loadUrl.toString());
        // Open devTool if the app is not packaged
        win.webContents.openDevTools({ mode: "detach" });
    } else {
        win.loadFile(indexHtml);
    }
    win.webContents.openDevTools({ mode: "detach" });

    // TODO: open up deviant art with this (this will open up the default browser)
    // Make all links open with the browser, not with the application
    // win.webContents.setWindowOpenHandler(({ url }) => {
    //     if (url.startsWith("https:")) shell.openExternal(url);
    //     return { action: "deny" };
    // });

    // logger.log("ELECTRON_APP_INITIALIZED");
    return win;
};
