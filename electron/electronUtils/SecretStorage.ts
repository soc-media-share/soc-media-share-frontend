import { safeStorage } from "electron";

import { join } from "path";
import { existsSync, promises } from "fs";

import { app } from "electron";

const readFile = promises.readFile;
const writeFile = promises.writeFile;

const userData = app.getPath("userData");
const storageFile = join(userData, "storage.json");

const secretDataKeys = ["deviantArtRefreshToken"];

export class SecretStorage {
    storage = {};
    storageLoaded = false;

    updateStorage = (key, data) => {
        this.storage[key] = data;
        this.saveStorage();
    };

    removeItemFromStorage = (key) => {
        delete this.storage[key];
        this.saveStorage();
    };

    checkIfStorageExists = () => existsSync(storageFile);

    initStorage = async () => {
        const newSecretStorage = JSON.stringify({});
        writeFile(storageFile, newSecretStorage, { encoding: "utf8" });
    };

    loadStorage = async () => {
        if (!this.checkIfStorageExists()) {
            this.initStorage();
            this.storage = {};
        } else {
            const storage = await readFile(storageFile, { encoding: "utf-8" });
            this.storage = this.decryptKeys(JSON.parse(storage));
        }

        this.storageLoaded = true;
    };

    getItem = (key) => {
        return this.storage[key];
    };

    saveStorage = () => {
        const newStorage = this.encryptKeys();
        writeFile(storageFile, JSON.stringify(newStorage), { encoding: "utf-8" });
    };

    encryptKeys = () => {
        const newStorage = { ...this.storage };
        const encryptedStorage = Object.keys(newStorage).reduce((prevValue, currentKey) => {
            if (secretDataKeys.includes(currentKey)) {
                return {
                    ...prevValue,
                    [currentKey]: this.encrypt(newStorage[currentKey]),
                };
            }

            return prevValue;
        }, {});

        return encryptedStorage;
    };

    decryptKeys = (crypticStorage) => {
        const newStorage = { ...crypticStorage };
        const decryptedStorage = Object.keys(newStorage).reduce((prevValue, currentKey) => {
            if (secretDataKeys.includes(currentKey)) {
                return {
                    ...prevValue,
                    [currentKey]: this.decrypt(newStorage[currentKey]),
                };
            }

            return prevValue;
        }, {});

        return decryptedStorage;
    };

    encrypt = (plainText) => {
        const buffer = safeStorage.encryptString(plainText);
        const encrypted = buffer.toString("base64");
        return encrypted;
    };

    decrypt = (encrypted) => {
        const buffer = Buffer.from(encrypted, "base64");
        const plainText = safeStorage.decryptString(buffer);
        return plainText;
    };
}
